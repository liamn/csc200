from gasp import *
import math

PLAYER_SPEED = 4
PLAYER_SIZE = 32
PLAYER_COLOR = (255, 255, 0)
endpoints = [0,315]


class Player:
    def __init__(self):
        self.x = 8*64
        self.y = 6*64
        self.d = 0
        self.shape = Arc(
            (self.x, self.y),
            PLAYER_SIZE,
            endpoints[0],
            endpoints[1],
            filled=True,
            color=PLAYER_COLOR
        )

    def place(self):
        self.shape.position = (self.x, self.y)
        self.shape.start_angle = endpoints [0]
        self.shape.end_angle = endpoints[1]

    def move(self):
        keys = keys_pressed()
        if 'left' in keys:
            self.d = 180
        if 'right' in keys:
            self.d = 0
        if 'up' in keys:
            self.d = 90
        if 'down' in keys:
            self.d = 270
        self.x += math.cos(math.radians(self.d)) * PLAYER_SPEED
        self.y += math.sin(math.radians(self.d)) * PLAYER_SPEED
        self.place()

begin_graphics(960, 540, "CHOMP", (0,0,0))

chompy = Player()

def gameiteration():
    endpoints[0] += 1
    endpoints[1] += 1
    chompy.move()
    if chompy.x >= 1000:
        finished = True

finished = False
while not finished:
    gameiteration()
    update_when("next_tick")

end_graphics()
