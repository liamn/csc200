import re


def getNumbers(polynomial):
    regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
    return regex.findall(polynomial)


print(getNumbers('2x^3+4x^2+8x-16'))
