from unittest import TestCase

from polynomials import Polynomial

# Graham wants to use the polynomial module to do such:
# >>> p1 = '2x^2 + 2x + 2'
# >>> p2 = '7x^3+3x^2+x'
# >>> p1 + p2
# >>> '7x^3 + 6x^2 + 3x + 2'

class PolynomialTesterSuite(TestCase):

    def setUp(self):
        self.p1 = Polynomial()

    def test_default_polynomial_is_0(self):
        self.assertTrue(str(self.p1), '0')
    
    def test_create_constant_polynomials(self):
        self.p2 = Polynomial('42')
        self.assertTrue(str(self.p2), '42')
        
    def test_polynomial_prints_nicely(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.p2 = Polynomial('7x^3+4x^2+x')
        
        self.assertEqual(str(self.p1), '2x^2 + 2x + 2')
        self.assertEqual(str(self.p2), '7x^3 + 4x^2 + x')

    def test_addition(self):
        self.p1 = Polynomial([(1, 2), (4, 0)])
        self.p2 = Polynomial([(2, 1), (5, 0)])
        
        self.assertEqual(str(self.p1 + self.p2), 'x^2 + 2x + 9')
