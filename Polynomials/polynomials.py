import re

def getTerms(polystr):

    regex = re.compile(r"([+-]*) *(\d+)?(x(\^(\d+))? *)?")
    raw_terms = regex.findall(polystr)[:-1]
    
    terms = []

    for i in raw_terms:
        if i[1]:
            coeff = float(i[1])
        else:
            coeff = 1

        if i[0] == '-':
            coeff = -coeff

        if i[2]:
            if i[4]:
                exp = int(i[4])
            else:
                exp = 1
        else:
            exp = 0

        terms.append((coeff, exp))

    return terms

class Polynomial:

    def __init__(self, poly='0'):
        if type(poly) == list:
            self.terms = poly
        else:
            self.terms = getTerms(poly)

    def __str__(self):

        poly_str = ''

        for term in self.terms:
            coeff, exp = term
            coeff_str = ''
            
            if coeff < 0:
                coeff_str += ' - '
                coeff *= -1
            else:
                coeff_str += ' + '
            
            if coeff != 1:
                if int(coeff) == coeff:
                    coeff = int(coeff)
                coeff_str += str(coeff)
            
            exp_str = ''
            
            if exp > 1:
                exp_str += f'x^{exp}'
            elif exp == 1:
                exp_str += 'x'

            poly_str += coeff_str + exp_str

        return poly_str[3:] if poly_str[1] == '+' else '-' + poly_str[3:]

    def __add__(self, other):
        exp1 = [i[1] for i in self.terms]
        exp2 = [i[1] for i in other.terms]

        deg1 = max(exp1)
        deg2 = max(exp2)

        deg = max(deg1, deg2)

        added = [0]*(deg + 1)
        
        set1 = self.terms
        set2 = other.terms

        for i in range(len(set1)):
            added[set1[i][1]] += set1[i][0]
        for i in range(len(set2)):
            added[set2[i][1]] += set2[i][0] 

        final = []
        for i in range(len(added)):
            final += [(added[deg - i], deg - i)]
        
        return Polynomial(final)
        
