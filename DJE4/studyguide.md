# STUDY GUIDE
**1. What is a protocol? Why does Dr. Chuck say it is important to understand web protocols before learning to be a web developer? What is HTTP?**

A protocol is a proper way of structuring something. It is important to understand web protocols for debugging.

**2. Describe the three parts illustrated in the slide titled Web Application Technologies. Be Specific. What makes up the left and the right parts? The middle part is represented as a cloud, and we won't be diving into the details of that part in our current study, but we will need to understand the two end parts of the system well.**

1st part: The browser. HTML, CSS, DOM, JavaScript, JQuery. It is what is used to show the website.
2nd part: The internet, which transfers data.
3rd part: Web server. Django, Flask, Sqlite3, MySQL.

**3. Describe what happens when you click on a link.**

When you click a link you send a GET request, to GET the content at that URL. The server responds by sending you the HTML document.

**4. What is a URL? What are its parts? Why does Dr. Chuck say this became the greatest thing ever?**

Protocol: usually http, tells the computer what to do to access the webpage.
Host: What website you are visiting
Document: The document that is loaded, found on the host website.
Dr. Chuck says this is the best thing ever because of how simple and intuitive it was.

**5. What is an RFC? What does Dr. Chuck say is important about the culture surrounding RFCs that helps make the internet what it is?**

An RFC is a request for comments. The important part of RFCs that helped make internet culture was how it revolved around communicating feedback.

**6. What does an HTTP reponse 200 mean? How about an HTTP response 404?**

HTTP response 200: The request succeeded.
HTTP response 404: Indicates the document could not be found.

**7. Dr. Chuck says he's a big fan of using the terminal. Why does he say this? What benefits does it provide?**

He says that fancy user-interfaces distract from the simplicity of what is really going on. The terminal is just as, if not more accessible.

**8. In the third video Dr. Chuck tells us that **phone calls** were the conceptual model used as the concept of computer networked communications was developed in the 1960s.**

**9. What is a socket? Describe in some detail what you learned about sockets from the video.**

A socket is a connection between two computers where they send information between each other but not with any other computer.

**10. What is a TCP port number?**

TCP sands for Transmission Control Protocol. It is a 16-bit number that helps tell what type of process is to be done.
