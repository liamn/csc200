#Experiments

#RECURSION

numbernames = ['zero', \
        'one', 'two', 'three', \
        'four', 'five', 'six', \
        'seven', 'eight', 'nine', 'ten']

def recursion(x):
    try:
        nn = numbernames[x]
        length = len(nn)
        print(f'{nn} has {numbernames[length]} letters')
        if x == 4:
            print('THE END')
            return None
        return recursion(length)
    except:
        print('NO! You should have entered a number between 0 and 10')
print('Enter a number between 0 and 10 to see its length')
recursion(int(input()))


#SETS

def makeaset():
    set = {2, 3, 4, 5, 6, 7, 8, 9}

def isitintheset(it, theset):
    assert type(theset) == set
    return it in theset

def setunion(set1, set2):
    #combines sets
    return (set1 | set2)

def setintersection(set1, set2):
    #only gets items that are in both sets
    return (set1 & set2)

def setsubtraction(set1, set2):
    #removes items from the first set if they are in the second set
    return (set1 - set2)

def setxor(set1, set2):
    #only returns items that are exclusively in one of the sets
    return (set1 ^ set2)

print(setintersection({1,2,3,4}, {2,3,4,5}))

def setrecursion(set1):
    import random
    set1.add(random.randint(0, 10))
    if len(set1) > 9:
        return set1
    else:
        return setrecursion(set1)

print(setrecursion({2}))

#ITERTOOLS
from itertools import count, accumulate, takewhile

def counttest():
    for i in count(3):
        print(i)
        if i >=11:
            break

def takewhiletest():
    nums = list(accumulate(range(8)))
    print(nums)
    print(list(takewhile(lambda x: x<= 6, nums)))
