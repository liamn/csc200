class Triangle:
    def __init__(self, x1, y1, x2, y2, x3, y3):    
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.y1 = y1
        self.y2 = y2
        self.y3 = y3
        self.base = ((x1 - x2) + (y1 - y2)) ** 0.5
        self.basemidpointx = (x1 + x2) / 2
        self.basemidpointy = (y1 + y2) / 2
        self.baseslope = (y2 - y1)/(x2 - x1)
        self.height = ((self.basemidpointx - x3) + (self.basemidpointy - y3) ** 0.5
        self.area = 0.5 * self.base * self.height

    def printarea(self):
        print(self.area)

triangle = Triangle(1,1, 2,1, 0,3)
triangle.printarea()
