class Base64Converter:
    def __init__(self):
        self.d = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        self.d += 'abcdefghijklmnopqrstuvwxyz0123456789+/'
        self.digpos = {}
        for pos, dig in enumerate(self.d):
            self.digpos[dig] = pos
        
    def encode(self, f):
        s = ''
        data = f.read(3)

        while data:
            s += self.encode3bytes(data)
            data = f.read(3)

        return s

    def decode(self, f):
        bs = bytes()
        data = f.read(4)
        while data:
            bs += self.decode4chars(data)
            data = f.read(4)

        return bs


    def encode3bytes(self, bytes3):
        if not isinstance(bytes3, bytes) or len(bytes3) < 1 or len(bytes3) > 3:
            raise ValueError('Input should be 1 to 3 bytes')
        d = self.digpos

        b1 = bytes3[0]
        index1 = b1 >> 2

        if len(bytes3) == 1:
            index2 = (b1 & 3) << 4
            return f'{self.d[index1]}{self.d[index2]}=='

        b2 = bytes3[1]
        index2 = (b1 & 3) << 4 | b2 >> 4

        if len(bytes3) == 2:
            index3 = (b2 & 15) << 2
            return f'{self.d[index1]}{self.d[index2]}{self.d[index3]}='

        b3 = bytes3[2]

        index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
        
        index4 = b3 & 63

        return f'{self.d[index1]}{self.d[index2]}{self.d[index3]}{self.d[index4]}'

    def decode4chars(self, s):

        if not isinstance(s, str) or len(s) != 4 or \
                not all([ch in self.d for ch in s[:2]]) or \
                not all([ch in self.d + '=' for ch in s[2:]]):
            raise ValueError(f'{s} is not a base64 encoded string')

        int1 = self.digpos[s[0]]
        int2 = self.digpos[s[1]]
        b1 = (int1 << 2) | ((int2 & 48) >> 4)

        if s[2:] == '==':
            return bytes([b1])

        int3 = self.digpos[s[2]]
        b2 = (int2 & 15) << 4 | int3 >> 2

        if s[3:] == '=':
            return bytes([b1, b2])

        int4 = self.digpos[s[3]]
        b3 = (int3 & 3) << 6 | int4

        return bytes([b1, b2, b3])









